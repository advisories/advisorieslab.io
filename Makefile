# Adapted from https://github.com/thockin/go-build-template
DBG_MAKEFILE ?=
ifeq ($(DBG_MAKEFILE),1)
	$(warning ***** starting Makefile for goal(s) "$(MAKECMDGOALS)")
	$(warning ***** $(shell date))
else
	# If we're not debugging the Makefile, don't echo recipes.
	MAKEFLAGS += -s
endif

# We don't need make's built-in rules.
MAKEFLAGS += --no-builtin-rules
# Be pedantic about undefined variables.
MAKEFLAGS += --warn-undefined-variables
.SUFFIXES:

SHELL := /usr/bin/env bash -o errexit -o pipefail -o nounset

.PHONY: build
build: # @HELP builds the site and search index to ./public
build: clean
	hugo --minify --gc
	npx pagefind --site public
	du -sh public

build-staging: # @HELP builds the site and search index for staging to ./public
build-staging: clean
	hugo --minify --gc --environment staging --baseURL "${BASE_URL}"
	npx pagefind --site public

build-test: # @HELP builds the site and search index with test content to ./public
build-test: clean
	hugo --gc --contentDir content-test
	npx pagefind --site public

test: # @HELP builds the site with test content and runs smoke tests
test: build-test
	npx playwright test

test-dev: # @HELP builds the site with test content and runs smoke tests with UI
test-dev: build-test
	npx playwright test --ui

clean: # @HELP removes build artifacts
clean:
	rm -rf public

preview: # @HELP serves built site in ./public
preview:
	npx serve public

.PHONY: content
content: # @HELP generates content from latest gemnasium-db advisories
content: clean-content
	./scripts/gen-content.sh

trim-content: # @HELP trims advisory content files to 500 random files for development
trim-content:
	find content -maxdepth 1 -type f -name '*.md' | shuf | tail -n +500 | xargs rm

clean-content: # @HELP removes generated advisory files in ./content
clean-content:
	rm -rf content
	mkdir content

help: # @HELP prints this message
help:
	echo
	echo "TARGETS:"
	grep -E '^.*: *# *@HELP' $(MAKEFILE_LIST)   \
	  | awk '                                   \
	    BEGIN {FS = ": *# *@HELP"};             \
	    { printf "  %-30s %s\n", $$1, $$2 };    \
		'

