# GitLab Advisory Database Website

This repository is home to the code for generating the static Hugo site at
<https://advisories.gitlab.com>.

# Installation and Development

## Generating content

Content files for the current advisories can be generated with `make content`:

```console
$ make content
Cloning into '/var/folders/g3/9pbs7wz93cn2g45b9c7vctlc0000gn/T/tmp.AJUcEzpr6V'...
remote: Enumerating objects: 40123, done.
remote: Counting objects: 100% (40123/40123), done.
remote: Compressing objects: 100% (25407/25407), done.
remote: Total 40123 (delta 8088), reused 36121 (delta 7434), pack-reused 0 (from 0)
Receiving objects: 100% (40123/40123), 15.93 MiB | 16.25 MiB/s, done.
Resolving deltas: 100% (8088/8088), done.
Updating files: 100% (28192/28192), done.
. . .
```

The command clones the latest [gemnasium-db](https://gitlab.com/gitlab-org/security-products/gemnasium-db) to a
temporary directory and generates Hugo content files for each advisory in `./content/`.

## Trimming content

The `make content` command will generate a large number of content files, which will slow down site generation
significantly during development. You can use `make trim-content` to trim the content files to a random selection of
500 files:

```console
$ make trim-content
```

## Building and previewing the site

When content has been generated with `make content` the site and search index can be built with `make build`:

```console
$ make build
Start building sites …
hugo v0.128.2+extended darwin/arm64 BuildDate=2024-07-04T08:13:25Z VendorInfo=brew


                   |  EN
-------------------+-------
  Pages            | 1528
  Paginator pages  |  396
  Non-page files   |    0
  Static files     |    1
  Processed images |    0
  Aliases          |    9
  Cleaned          |    0

Total in 617 ms

Running Pagefind v1.1.0 (Extended)
Running from: "/Volumes/sourcecode/gitlab.com/gitlab-org/secure/vulnerability-research/advisories/landing-page"
Source:       "public"
Output:       "public/pagefind"

. . .

[Building search indexes]
Total:
  Indexed 1 language
  Indexed 277 pages
  Indexed 3013 words
  Indexed 4 filters
  Indexed 1 sort

Finished in 0.967 seconds
 26M    public

```

After the site is built, it can be previewed locally with `make preview`:

```console
$ make preview

   ┌─────────────────────────────────────────┐
   │                                         │
   │   Serving!                              │
   │                                         │
   │   - Local:    http://localhost:3000     │
   │   - Network:  http://10.0.36.165:3000   │
   │                                         │
   │   Copied local address to clipboard!    │
   │                                         │
   └─────────────────────────────────────────┘

```

## Development

The [Hugo](https://gohugo.io/) theme resides in `./themes/gl-adv-db/`, where you'll find the layout templates and static
assets of the site.

The frontend uses [Tailwindcss](https://tailwindcss.com/) as the CSS framework and will need to be compiled during
development. Use `npm run dev` inside `./themes/gl-adv-db` to run Hugo site building and Tailwind compilation in
parallel when files are changed:

```
$ npm run dev

> dev
> run-p dev-tailwind dev-hugo


> dev-tailwind
> tailwindcss -i assets/css/main.css -o assets/css/index.css --watch


> dev-hugo
> hugo server --ignoreCache -s ../..

. . .

                   |  EN
-------------------+-------
  Pages            | 1528
  Paginator pages  |  396
  Non-page files   |    0
  Static files     |    1
  Processed images |    0
  Aliases          |    9
  Cleaned          |    0

Built in 427 ms
Environment: "development"
Serving pages from disk
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

### Running tests

The project contains a few smoke tests to assert that important elements are present on a built site.
[Playwright](https://playwright.dev/) is used as the testing framework, and the tests themselves can be found in
`./tests/smoke.spec.ts`.

Tests can be run with `make test` which will build the site with content from `./content-test` and run Playwright:

```console
$ make test
Start building sites …
hugo v0.128.2+extended darwin/arm64 BuildDate=2024-07-04T08:13:25Z VendorInfo=brew


                   | EN
-------------------+-----
  Pages            | 21
  Paginator pages  |  0
  Non-page files   |  0
  Static files     |  1
  Processed images |  0
  Aliases          |  1
  Cleaned          |  0

Total in 22 ms

. . .

Running 20 tests using 5 workers

  ✓  1 [chromium] › smoke.spec.ts:13:9 › Home › Head › has title (369ms)
  ✓  2 [chromium] › smoke.spec.ts:17:9 › Home › Head › has meta description (461ms)
  ✓  3 [chromium] › smoke.spec.ts:33:9 › Home › Nav bar › is visible (476ms)
  ✓  4 [chromium] › smoke.spec.ts:37:9 › Home › Nav bar › has search input (481ms)
  ✓  5 [chromium] › smoke.spec.ts:24:9 › Home › Head › has canonical link (375ms)
  ✓  6 [chromium] › smoke.spec.ts:44:11 › Home › Nav bar › Menu › has link to advisories (219ms)
  ✓  7 [chromium] › smoke.spec.ts:50:11 › Home › Nav bar › Menu › has link to Dependency Scanning (196ms)
  ✓  8 [chromium] › smoke.spec.ts:62:9 › Home › Body › has page heading (195ms)
  ✓  9 [chromium] › smoke.spec.ts:71:9 › Home › Body › links to CVE-2024-65432 (189ms)
  ✓  10 [chromium] › smoke.spec.ts:85:9 › Home › Body › links to CVE-2023-12345 (199ms)
  ✓  11 [chromium] › smoke.spec.ts:100:11 › Home › Body › Footer › links to About GitLab (159ms)
  ✓  12 [chromium] › smoke.spec.ts:106:11 › Home › Body › Footer › links to Terms (146ms)
  ✓  13 [chromium] › smoke.spec.ts:112:11 › Home › Body › Footer › links to Privacy Statement (169ms)
  ✓  14 [chromium] › smoke.spec.ts:127:9 › Advisory page › Head › has title (191ms)
  ✓  15 [chromium] › smoke.spec.ts:133:9 › Advisory page › Head › has meta description (176ms)
  ✓  16 [chromium] › smoke.spec.ts:140:9 › Advisory page › Head › has canonical link (142ms)
  ✓  17 [chromium] › smoke.spec.ts:149:9 › Advisory page › Body › has page heading (144ms)
  ✓  18 [chromium] › smoke.spec.ts:158:9 › Advisory page › Body › has publication date (154ms)
  ✓  19 [chromium] › smoke.spec.ts:164:9 › Advisory page › Body › has updated date (144ms)
  ✓  20 [chromium] › smoke.spec.ts:170:9 › Advisory page › Body › has advisory description (149ms)

  20 passed (1.8s)

```
