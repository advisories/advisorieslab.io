package advisory

import (
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"slices"
	"strings"

	gocvss20 "github.com/pandatix/go-cvss/20"
	gocvss30 "github.com/pandatix/go-cvss/30"
	gocvss31 "github.com/pandatix/go-cvss/31"
	"gopkg.in/yaml.v3"
)

// Link represents a reference link for an advisory.
type Link struct {
	URL  string `yaml:"url"`
	Type string `yaml:"type,omitempty"`
}

// Commit represents version commit information.
type Commit struct {
	Tags      []string `yaml:"tags"`
	SHA       string   `yaml:"sha"`
	Timestamp string   `yaml:"timestamp"`
}

// Version represents version meta information for an advisory.
type Version struct {
	Number string `yaml:"number"`
	Commit Commit `yaml:"commit"`
}

type cvssData struct {
	vector string
	score  float64
	rating string
}

// Raw represents a raw advisory from Gemnasium DB.
type Raw struct {
	SHA              string    `yaml:"-"`
	Identifier       string    `yaml:"identifier"`
	Identifiers      []string  `yaml:"identifiers"`
	PackageSlug      string    `yaml:"package_slug"`
	Title            string    `yaml:"title"`
	Description      string    `yaml:"description"`
	Date             string    `yaml:"date"`
	AffectedRange    string    `yaml:"affected_range"`
	FixedVersions    []string  `yaml:"fixed_versions"`
	AffectedVersions string    `yaml:"affected_versions"`
	URLs             []string  `yaml:"urls"`
	UUID             string    `yaml:"uuid"`
	PubDate          string    `yaml:"pubdate"`
	Credit           string    `yaml:"credit,omitempty"`
	CWEIDs           []string  `yaml:"cwe_ids,omitempty"`
	CVSSv2           string    `yaml:"cvss_v2,omitempty"`
	CVSSv3           string    `yaml:"cvss_v3,omitempty"`
	Solution         string    `yaml:"solution,omitempty"`
	NotImpacted      string    `yaml:"not_impacted,omitempty"`
	Versions         []Version `yaml:"versions,omitempty"`
	Links            []Link    `yaml:"links,omitempty"`
}

// FromReader creates a new raw advisory from provided reader expected to
// contain YAML encoded advisory data.
func FromReader(r io.Reader) (*Raw, error) {
	h := sha256.New()
	tr := io.TeeReader(r, h)

	data, err := io.ReadAll(tr)
	if err != nil {
		return nil, fmt.Errorf("reading data: %w", err)
	}

	var adv Raw
	if err := yaml.Unmarshal(data, &adv); err != nil {
		return nil, fmt.Errorf("decoding data: %w", err)
	}

	adv.SHA = base64.StdEncoding.EncodeToString(h.Sum(nil))
	adv.PackageSlug = strings.TrimSuffix(adv.PackageSlug, "/")

	return &adv, nil
}

func (a *Raw) cvss() (cvssData, error) {
	d := cvssData{}

	switch {
	case a.CVSSv3 != "":
		d.vector = strings.ToUpper(a.CVSSv3)
		if !strings.HasPrefix(d.vector, "CVSS:") {
			d.vector = "CVSS:3.0/" + d.vector
		}
	case a.CVSSv2 != "":
		d.vector = strings.Replace(strings.ToUpper(a.CVSSv2), "AU", "Au", 1)
	default:
		return d, nil
	}

	switch {
	case strings.HasPrefix(d.vector, "CVSS:3.1"):
		cvss, err := gocvss31.ParseVector(d.vector)
		if err != nil {
			return d, fmt.Errorf("parsing CVSS 3.1 vector: %w", err)
		}

		d.score = cvss.BaseScore()
		d.rating, _ = gocvss31.Rating(d.score)
	case strings.HasPrefix(d.vector, "CVSS:3.0"):
		cvss, err := gocvss30.ParseVector(d.vector)
		if err != nil {
			return d, fmt.Errorf("parsing CVSS 3.0 vector: %w", err)
		}

		d.score = cvss.BaseScore()
		d.rating, _ = gocvss30.Rating(d.score)
	default:
		cvss, err := gocvss20.ParseVector(d.vector)
		if err != nil {
			return d, fmt.Errorf("parsing CVSS 2.0 vector: %w", err)
		}

		d.score = cvss.BaseScore()
		d.rating, _ = gocvss30.Rating(d.score)
	}

	return d, nil
}

func (a *Raw) allURLs() []string {
	uMap := make(map[string]struct{}, len(a.URLs))

	for _, u := range a.URLs {
		uMap[u] = struct{}{}
	}

	for _, l := range a.Links {
		uMap[l.URL] = struct{}{}
	}

	urls := make([]string, 0, len(uMap))
	for u := range uMap {
		urls = append(urls, u)
	}

	slices.Sort(urls)

	return urls
}
