package advisory

import (
	"bytes"
	"fmt"
	"log/slog"
	"net/url"
	"path/filepath"
	"regexp"
	"slices"
	"strings"

	"github.com/microcosm-cc/bluemonday"
	"github.com/yuin/goldmark"
)

const summaryWordCount = 70

var slugRE = regexp.MustCompile(`[^\w_]+`)

var excludedCWEs = map[string]struct{}{
	"CWE-1035": {},
	"CWE-937":  {},
}

// FrontMatter represents the Markdown front matter for an advisory content
// file rendered with Hugo.
type FrontMatter struct {
	Title        string            `yaml:"title"`
	URL          string            `yaml:"url"`
	Aliases      []string          `yaml:"aliases,omitempty,flow"`
	Date         string            `yaml:"date"`
	LastModified string            `yaml:"lastmod"`
	Summary      string            `yaml:"summary"`
	Packages     []string          `yaml:"packages,flow"`
	Type         string            `yaml:"type"`
	Params       FrontMatterParams `yaml:"params"`
}

func NewFrontMatter(adv *Raw) (*FrontMatter, error) {
	plainDesc, err := plainify(adv.Description)
	if err != nil {
		return nil, fmt.Errorf("cleaning description: %w", err)
	}

	purl, err := makePURL(adv.PackageSlug)
	if err != nil {
		return nil, fmt.Errorf("creating package URL: %w", err)
	}

	cvss, err := adv.cvss()
	if err != nil {
		slog.Warn("failed parsing CVSS vector", "error", err, "vector", cvss.vector, "advisory", adv.Identifier)
	}

	ecosystem, _, _ := strings.Cut(strings.TrimPrefix(purl, "pkg:"), "/")
	_, pkgName, _ := strings.Cut(adv.PackageSlug, "/")

	params := FrontMatterParams{
		SHA:              adv.SHA,
		SourceFile:       filepath.Join(adv.PackageSlug, adv.Identifier+".yml"),
		Identifier:       adv.Identifier,
		Ecosystem:        ecosystem,
		PackageName:      pkgName,
		PURL:             purl,
		AffectedRange:    adv.AffectedRange,
		AffectedVersions: adv.AffectedVersions,
		FixedVersions:    adv.FixedVersions,
		Solution:         adv.Solution,
		Credit:           adv.Credit,
		URLs:             adv.allURLs(),
		CVSS:             cvss.vector,
		CVSSScore:        cvss.score,
		CVSSRating:       cvss.rating,
	}

	for _, id := range adv.Identifiers {
		if id == adv.Identifier {
			continue
		}
		params.Identifiers = append(params.Identifiers, id)
	}

	for _, cwe := range adv.CWEIDs {
		if _, excluded := excludedCWEs[cwe]; excluded {
			continue
		}

		if _, n, ok := strings.Cut(cwe, "-"); ok {
			params.CWEs = append(params.CWEs, n)
		}
	}

	slices.Sort(params.CWEs)

	fm := FrontMatter{
		Title:        adv.Title,
		URL:          makePath(ecosystem, pkgName, adv.Identifier),
		Aliases:      []string{makeLegacyPath(adv)},
		Date:         adv.PubDate,
		LastModified: adv.Date,
		Summary:      makeSummary(plainDesc, summaryWordCount),
		Packages:     []string{ecosystem + "/" + pkgName},
		Type:         "advisories",
		Params:       params,
	}

	return &fm, nil
}

// FrontMatterParams represents arbitrary [FrontMatter] parameters.
type FrontMatterParams struct {
	SHA              string   `yaml:"sha"`
	SourceFile       string   `yaml:"source_file"`
	Identifier       string   `yaml:"identifier"`
	Identifiers      []string `yaml:"identifiers"`
	Ecosystem        string   `yaml:"ecosystem"`
	PackageName      string   `yaml:"package_name"`
	CWEs             []string `yaml:"cwes,omitempty,flow"`
	PURL             string   `yaml:"purl"`
	AffectedRange    string   `yaml:"affected_range"`
	AffectedVersions string   `yaml:"affected_versions"`
	FixedVersions    []string `yaml:"fixed_versions,flow"`
	Solution         string   `yaml:"solution,omitempty"`
	Credit           string   `yaml:"credit,omitempty"`
	URLs             []string `yaml:"urls,omitempty"`
	CVSS             string   `yaml:"cvss,omitempty"`
	CVSSScore        float64  `yaml:"cvss_score,omitempty"`
	CVSSRating       string   `yaml:"cvss_rating,omitempty"`
}

func plainify(text string) (string, error) {
	var buf bytes.Buffer
	if err := goldmark.Convert([]byte(strings.TrimSpace(text)), &buf); err != nil {
		return "", fmt.Errorf("converting Markdown to HTML: %w", err)
	}

	return bluemonday.StrictPolicy().SanitizeReader(&buf).String(), nil
}

func makeSummary(plain string, wordCount int) string {
	words := strings.Fields(plain)
	if len(words) > wordCount {
		return strings.Join(words[:wordCount], " ") + " …"
	}

	return strings.Join(words, " ")
}

func makePURL(pkgSlug string) (string, error) {
	ecosystem, pkg, ok := strings.Cut(pkgSlug, "/")
	if !ok {
		return "", fmt.Errorf("invalid package slug %q", pkgSlug)
	}

	switch ecosystem {
	case "go":
		ecosystem = "golang"
	case "packagist":
		ecosystem = "composer"
	}

	return fmt.Sprintf("pkg:%s/%s", ecosystem, strings.ReplaceAll(url.QueryEscape(pkg), "%2F", "/")), nil
}

func makePath(ecosystem, pkgName, identifier string) string {
	return fmt.Sprintf("/pkg/%s/%s/%s/", ecosystem, strings.ToLower(pkgName), identifier)
}

// makeLegacyPath creates a path to an advisory as it would be on the legacy
// advisory database site:
//
// /pkg/golang/github.com/johndoe/project/CVE-2024-12345 => /advisory/advgo_github_com_johndoe_project_CVE_2024_12345.html
func makeLegacyPath(adv *Raw) string {
	return fmt.Sprintf("/advisory/adv%s_%s.html", legacySlug(adv.PackageSlug), legacySlug(adv.Identifier))
}

func legacySlug(s string) string {
	return slugRE.ReplaceAllString(s, "_")
}
