package advisory

import (
	"fmt"
	"regexp"
	"slices"
	"strings"

	"github.com/jdkato/prose/v2"
)

const (
	kwMinLength = 4
	kwMaxLength = 14
)

// POS tags suitable for keywords.
var kwPOSTags = map[string]struct{}{
	"NN":   {}, // noun, singular or mass.
	"NNP":  {}, // noun, proper singular.
	"NNPS": {}, // noun, proper plural.
	"NNS":  {}, // noun, plural.
}

var kwRE = regexp.MustCompile(`^[a-z0-9._-]+$`)

// excludedKws is a map of common words in advisories that don't provide value
// as keywords.
var excludedKws = map[string]struct{}{
	"ability":        {},
	"accesses":       {},
	"actor":          {},
	"actors":         {},
	"address":        {},
	"addresses":      {},
	"advisory":       {},
	"allows":         {},
	"amount":         {},
	"anyone":         {},
	"anything":       {},
	"application":    {},
	"applications":   {},
	"attack":         {},
	"attacker":       {},
	"attackers":      {},
	"attempt":        {},
	"attempts":       {},
	"behavior":       {},
	"behaviors":      {},
	"bugs":           {},
	"cannot":         {},
	"case":           {},
	"cause":          {},
	"check":          {},
	"checks":         {},
	"circumstance":   {},
	"circumstances":  {},
	"circumvent":     {},
	"code":           {},
	"condition":      {},
	"conditions":     {},
	"configuration":  {},
	"configurations": {},
	"consequence":    {},
	"consequences":   {},
	"consideration":  {},
	"considerations": {},
	"constraint":     {},
	"constraints":    {},
	"cross":          {},
	"custom":         {},
	"data":           {},
	"default":        {},
	"detail":         {},
	"details":        {},
	"e.g.":           {},
	"enforce":        {},
	"entirety":       {},
	"equals":         {},
	"error":          {},
	"errors":         {},
	"example":        {},
	"exists":         {},
	"fail":           {},
	"fails":          {},
	"file":           {},
	"flaw":           {},
	"functions":      {},
	"impact":         {},
	"implementation": {},
	"interaction":    {},
	"issue":          {},
	"issuer":         {},
	"length":         {},
	"line":           {},
	"master":         {},
	"mishandle":      {},
	"mishandles":     {},
	"mitigation":     {},
	"mitigations":    {},
	"none":           {},
	"note":           {},
	"nothing":        {},
	"number":         {},
	"party":          {},
	"purpose":        {},
	"read":           {},
	"reasons":        {},
	"reference":      {},
	"results":        {},
	"risk":           {},
	"risks":          {},
	"scenario":       {},
	"security":       {},
	"service":        {},
	"situations":     {},
	"someone":        {},
	"summary":        {},
	"support":        {},
	"target":         {},
	"threat":         {},
	"user":           {},
	"users":          {},
	"uses":           {},
	"value":          {},
	"vector":         {},
	"version":        {},
	"versions":       {},
	"victim":         {},
	"vulnerability":  {},
	"workaround":     {},
	"workarounds":    {},
}

func extractKeywords(plain string) ([]string, error) {
	var kws []string

	doc, err := prose.NewDocument(strings.ToLower(plain))
	if err != nil {
		return kws, fmt.Errorf("processing text: %w", err)
	}

	for _, tok := range doc.Tokens() {
		if _, ok := kwPOSTags[tok.Tag]; !ok {
			continue
		}

		if len(tok.Text) < kwMinLength || len(tok.Text) > kwMaxLength {
			continue
		}

		if _, excluded := excludedKws[tok.Text]; excluded {
			continue
		}

		if !kwRE.MatchString(tok.Text) {
			continue
		}

		kws = append(kws, tok.Text)
	}

	return slices.Compact(kws), nil
}
