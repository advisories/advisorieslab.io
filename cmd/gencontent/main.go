package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"gopkg.in/yaml.v3"

	"gitlab.com/gitlab-org/secure/vulnerability-research/advisories/landing-page/cmd/gencontent/internal/advisory"
)

var (
	destFlag   = flag.String("dest", "content", "base destination directory for generated content files")
	dryrunFlag = flag.Bool("dry-run", false, "run simulated content file creation")
)

var ecosystems = []string{"conan", "gem", "go", "maven", "npm", "nuget", "packagist", "pypi", "swift", "cargo"}

func main() {
	flag.Parse()
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, nil)))

	sourceDir := flag.Arg(0)
	if sourceDir == "" {
		fmt.Fprintf(os.Stderr, "usage: %s [flags] <gemnasium-db project path>\n\n", os.Args[0])
		flag.Usage()
		os.Exit(1)
	}

	sourceDir, err := filepath.Abs(sourceDir)
	if err != nil {
		fatal("converting source directory to absolute path", err)
	}

	mustDir(sourceDir)
	mustDir(*destFlag)

	wg := sync.WaitGroup{}

	for _, ecosystem := range ecosystems {
		root := filepath.Join(sourceDir, ecosystem)
		if !dirExists(root) {
			continue
		}

		wg.Add(1)

		go func() {
			err := filepath.Walk(root, func(path string, info fs.FileInfo, err error) error {
				if err != nil {
					return err
				}

				if filepath.Ext(path) != ".yml" {
					return nil
				}

				f, err := os.Open(path)
				if err != nil {
					return fmt.Errorf("opening advisory file: %w", err)
				}
				defer f.Close()

				adv, err := advisory.FromReader(f)
				if err != nil {
					return fmt.Errorf("loading advisory %s: %w", path, err)
				}

				destName := filepath.Join(*destFlag, adv.UUID+".md")
				destSHA, err := contentSHA(destName)
				if err != nil {
					if errors.Is(err, fs.ErrNotExist) {
						n, err := writeContent(adv, destName)
						if err != nil {
							slog.Error("failed creating content file", "error", err, "path", destName)
							return nil
						}

						slog.Debug("created content file", "path", destName, "bytes", n, "dryRun", *dryrunFlag)
						return nil
					}

					return fmt.Errorf("getting content SHA for %s: %w", adv.PackageSlug, err)
				}

				if destSHA == adv.SHA {
					slog.Debug("content file is up-to-date", "path", destName)
					return nil
				}

				n, err := writeContent(adv, destName)
				if err != nil {
					slog.Error("failed updating content file", "error", err, "path", destName)
					return nil
				}

				slog.Debug("updated content file", "path", destName, "bytes", n, "dryRun", *dryrunFlag)

				return nil
			})
			if err != nil {
				fatal("processing advisories", err)
			}

			wg.Done()
		}()
	}

	wg.Wait()
}

func writeContent(adv *advisory.Raw, name string) (int, error) {
	frontmatter, err := advisory.NewFrontMatter(adv)
	if err != nil {
		return 0, fmt.Errorf("creating front matter: %w", err)
	}

	encoded, err := yaml.Marshal(frontmatter)
	if err != nil {
		return 0, fmt.Errorf("encoding front matter: %w", err)
	}

	var w io.Writer

	if *dryrunFlag {
		w = io.Discard
	} else {
		if err := os.MkdirAll(filepath.Dir(name), 0o744); err != nil {
			return 0, fmt.Errorf("creating destination directory: %w", err)
		}

		f, err := os.Create(name)
		if err != nil {
			return 0, fmt.Errorf("opening destination file for writing: %w", err)
		}
		defer f.Close()

		w = f
	}

	n, err := fmt.Fprintf(w, "---\n%s---\n\n%s\n\n", encoded, strings.TrimSpace(adv.Description))
	if err != nil {
		return n, fmt.Errorf("writing to file: %w", err)
	}

	return n, nil
}

func fatal(msg string, err error, args ...any) {
	if err != nil {
		args = append(args, "error", err)
	}

	slog.Error("fatal: "+msg, args...)
	os.Exit(1)
}

func dirExists(name string) bool {
	stat, err := os.Stat(name)
	if err != nil {
		return false
	}

	return stat.IsDir()
}

func mustDir(path string) {
	stat, err := os.Stat(path)
	if err != nil {
		fatal("getting file information", err, "path", path)
	}

	if !stat.IsDir() {
		fatal("path is not a directory", nil, "path", path)
	}
}

func contentSHA(name string) (string, error) {
	b, err := os.ReadFile(name)
	if err != nil {
		return "", fmt.Errorf("reading file: %w", err)
	}

	var ok bool
	b, ok = bytes.CutPrefix(b, []byte("---"))
	if !ok {
		return "", errors.New("invalid content: expected YAML front matter")
	}

	data, _, ok := bytes.Cut(b, []byte("---"))
	if !ok {
		return "", errors.New("invalid content: expected YAML front matter")
	}

	var fm advisory.FrontMatter
	if err := yaml.Unmarshal(data, &fm); err != nil {
		return "", fmt.Errorf("decoding front matter: %w", err)
	}

	return fm.Params.SHA, nil
}
