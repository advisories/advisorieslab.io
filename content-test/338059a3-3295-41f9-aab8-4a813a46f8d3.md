---
title: Cross-Site Scripting (XSS) Vulnerability in WebApp Framework
url: /pkg/npm/github.com/alice/webapp-framework/CVE-2024-98765
date: "2024-05-10"
lastmod: "2024-05-12"
summary: A Cross-Site Scripting (XSS) vulnerability (CVE-2024-98765) was discovered in the WebApp Framework, allowing attackers to inject malicious scripts.
packages: [npm/github.com/alice/webapp-framework]
type: advisories
params:
    sha: Z2VuZXJhdGVkdmFsdWU=
    source_file: npm/github.com/alice/webapp-framework/CVE-2024-98765.yml
    identifier: CVE-2024-98765
    identifiers:
        - GHSA-abcd-efgh-ijkl
    ecosystem: npm
    package_name: github.com/alice/webapp-framework
    cwes: ["79"]
    purl: pkg:npm/webapp-framework@1.8.0
    affected_range: "<1.8.0"
    affected_versions: All versions before 1.8.0
    fixed_versions: [1.8.0]
    solution: Upgrade to version 1.8.0 or above.
    urls:
        - https://github.com/advisories/GHSA-abcd-efgh-ijkl
        - https://github.com/alice/webapp-framework/releases/tag/v1.8.0
        - https://nvd.nist.gov/vuln/detail/CVE-2024-98765
    cvss: CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N
    cvss_score: 6.1
    cvss_rating: MEDIUM
---

A Cross-Site Scripting (XSS) vulnerability (CVE-2024-98765) has been identified in the WebApp Framework, a popular Node.js-based framework for building web applications. The vulnerability allows attackers to inject malicious scripts into web pages, which could be executed in the context of another user’s browser session. This could lead to the theft of session tokens, sensitive information, or other malicious actions.

The vulnerability affects all versions of WebApp Framework prior to version 1.8.0. It was discovered by a security researcher who noticed that user input was not being properly sanitized before rendering. As a result, attackers could exploit this flaw by injecting specially crafted payloads into certain fields or URLs.

To mitigate this risk, users should upgrade to version 1.8.0 or later. This release includes a fix that ensures proper sanitization of all user inputs, thereby preventing XSS attacks. The vulnerability has been given a CVSS score of 6.1, marking it as medium severity, but it could have a significant impact depending on the application’s context.
