---
title: SQL Injection Vulnerability in DataProcessor
url: /pkg/pypi/github.com/janedoe/dataprocessor/CVE-2024-56789
date: "2024-03-14"
summary: An SQL injection vulnerability (CVE-2024-56789) was found in DataProcessor, potentially allowing attackers to execute arbitrary SQL commands.
packages: [pypi/github.com/janedoe/dataprocessor]
type: advisories
params:
    sha: Y29tcHJvbWlzaW5ndmFsdWU=
    source_file: pypi/github.com/janedoe/dataprocessor/CVE-2024-56789.yml
    identifier: CVE-2024-56789
    identifiers:
        - GHSA-7g8h-9i0j-klmn
    ecosystem: pypi
    package_name: github.com/janedoe/dataprocessor
    cwes: ["89"]
    purl: pkg:pypi/dataprocessor@2.4.1
    affected_range: "<2.4.1"
    affected_versions: All versions before 2.4.1
    fixed_versions: [2.4.1]
    solution: Upgrade to version 2.4.1 or above.
    urls:
        - https://github.com/advisories/GHSA-7g8h-9i0j-klmn
        - https://github.com/janedoe/dataprocessor/releases/tag/v2.4.1
        - https://nvd.nist.gov/vuln/detail/CVE-2024-56789
    cvss: CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
    cvss_score: 9.8
    cvss_rating: CRITICAL
---

A severe SQL injection vulnerability (CVE-2024-56789) has been identified in the DataProcessor library, which is widely used for data management tasks. The vulnerability stems from the improper sanitization of user inputs in certain API endpoints, which allows attackers to inject and execute arbitrary SQL commands. This could lead to unauthorized data access, data manipulation, or even complete database compromise.

This flaw affects all versions of DataProcessor prior to v2.4.1. The issue was reported by a third-party security researcher and promptly addressed by the maintainers. Users are strongly advised to upgrade to version 2.4.1 or higher. Failure to apply the update leaves systems vulnerable to potential exploitation. The vulnerability has been rated as critical, with a CVSS score of 9.8. Relevant details and references are provided in the official advisories.
