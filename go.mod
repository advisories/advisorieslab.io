module gitlab.com/gitlab-org/secure/vulnerability-research/advisories/landing-page

go 1.22

require (
	github.com/jdkato/prose/v2 v2.0.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/goark/errs v1.3.2 // indirect
	github.com/gorilla/css v1.0.1 // indirect
	golang.org/x/net v0.26.0 // indirect
)

require (
	github.com/deckarep/golang-set v1.7.1 // indirect
	github.com/goark/go-cvss v1.6.7
	github.com/microcosm-cc/bluemonday v1.0.27
	github.com/mingrammer/commonregex v1.0.1 // indirect
	github.com/pandatix/go-cvss v0.6.2
	github.com/yuin/goldmark v1.7.4
	gonum.org/v1/gonum v0.7.0 // indirect
	gopkg.in/neurosnap/sentences.v1 v1.0.6 // indirect
)
