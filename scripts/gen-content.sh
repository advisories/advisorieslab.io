#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

tempdir="$(mktemp -d)"
git clone --depth 1 "https://gitlab.com/gitlab-org/security-products/gemnasium-db.git" "$tempdir"

go run cmd/gencontent/main.go "$tempdir"
rm -rf "$tempdir"
