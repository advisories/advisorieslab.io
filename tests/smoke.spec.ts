import { test, expect } from "@playwright/test";

const EXPECTED_DESCRIPTION =
  "Explore the GitLab Advisory Database for security advisories related to software dependencies in your projects.";

const GTM_ID = "GTM-NJXWQL";
const OT_ID = "7f944245-c5cd-4eed-a90e-dd955adfdd08";
const G_VERIFICATION_ID = "BMTt3nUg8E52CZeY2arQ56raq5azFM2XxQudrL78rsM";

test.describe("Home", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("/");
  });

  test.describe("Head", () => {
    test("has title", async ({ page }) => {
      await expect(page).toHaveTitle(/GitLab Advisory Database/);
    });

    test("has meta description", async ({ page }) => {
      await expect(page.locator("meta[name=description]")).toHaveAttribute(
        "content",
        EXPECTED_DESCRIPTION,
      );
    });

    test("has canonical link", async ({ page }) => {
      await expect(page.locator("link[rel=canonical]")).toHaveAttribute(
        "href",
        "https://advisories.gitlab.com/",
      );
    });

    test("has Google site verification", async ({ page }) => {
      await expect(
        page.locator("meta[name=google-site-verification]"),
      ).toHaveAttribute("content", G_VERIFICATION_ID);
    });

    test("loads Google Analytics", async ({ page }) => {
      const scripts = page.locator("script");
      const srcList = await scripts.evaluateAll((elements) =>
        elements.map((el) => el.getAttribute("src")),
      );
      expect(srcList).toContain(
        `https://www.googletagmanager.com/gtm.js?id=${GTM_ID}`,
      );
    });

    test("loads OneTrust cookie consent", async ({ page }) => {
      const scripts = page.locator("script");
      const srcList = await scripts.evaluateAll((elements) =>
        elements.map((el) => el.getAttribute("src")),
      );
      expect(srcList).toContain(
        `https://cdn.cookielaw.org/consent/${OT_ID}/OtAutoBlock.js`,
      );
      expect(srcList).toContain(
        "https://cdn.cookielaw.org/scripttemplates/otSDKStub.js",
      );
      expect(srcList).toContain(
        "https://cdn.cookielaw.org/scripttemplates/202407.2.0/otBannerSdk.js",
      );
    });
  });

  test.describe("Nav bar", () => {
    test("is visible", async ({ page }) => {
      await expect(page.locator("header").first()).toBeVisible();
    });

    test("has search input", async ({ page }) => {
      const el = page.locator("header .pagefind-ui__search-input");
      await expect(el).toBeVisible();
      await expect(el).toHaveAttribute("placeholder", "Search");
    });

    test.describe("Menu", () => {
      test("has link to advisories", async ({ page }) => {
        await expect(
          page.getByRole("link", { name: "Advisories", exact: true }),
        ).toHaveAttribute("href", "/advisories/");
      });

      test("has link to Dependency Scanning", async ({ page }) => {
        await expect(
          page.getByRole("link", { name: "Dependency Scanning", exact: true }),
        ).toHaveAttribute(
          "href",
          "https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html",
        );
      });
    });
  });

  test.describe("Body", () => {
    test("has page heading", async ({ page }) => {
      await expect(
        page.getByRole("heading", {
          name: "GitLab Advisory Database",
          exact: true,
        }),
      ).toBeVisible();
    });

    test("links to CVE-2024-65432", async ({ page }) => {
      await expect(
        page
          .getByRole("link", {
            name: "Denial of Service Vulnerability in FastAPI Library",
            exact: true,
          })
          .first(),
      ).toHaveAttribute(
        "href",
        "/pkg/pypi/github.com/devjoe/fastapi-lib/CVE-2024-65432",
      );
    });

    test("links to CVE-2023-12345", async ({ page }) => {
      await expect(
        page
          .getByRole("link", {
            name: "Potential Remote Code Execution Vulnerability in MyProject",
            exact: true,
          })
          .first(),
      ).toHaveAttribute(
        "href",
        "/pkg/golang/github.com/johndoe/myproject/CVE-2023-12345",
      );
    });

    test.describe("Footer", () => {
      test("links to About GitLab", async ({ page }) => {
        await expect(
          page.locator("footer a").filter({ hasText: "About GitLab" }),
        ).toHaveAttribute("href", "https://about.gitlab.com/company/");
      });

      test("links to Terms", async ({ page }) => {
        await expect(
          page.locator("footer a").filter({ hasText: "Terms" }),
        ).toHaveAttribute("href", "https://about.gitlab.com/terms/");
      });

      test("links to Privacy Statement", async ({ page }) => {
        await expect(
          page.locator("footer a").filter({ hasText: "Privacy Statement" }),
        ).toHaveAttribute("href", "https://about.gitlab.com/privacy/");
      });
    });
  });
});

test.describe("Advisory page", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("/pkg/pypi/github.com/devjoe/fastapi-lib/CVE-2024-65432");
  });

  test.describe("Head", () => {
    test("has title", async ({ page }) => {
      await expect(page).toHaveTitle(
        /Denial of Service Vulnerability in FastAPI Library/,
      );
    });

    test("has meta description", async ({ page }) => {
      await expect(page.locator("meta[name=description]")).toHaveAttribute(
        "content",
        /CVE-2024-65432 Denial of Service Vulnerability in FastAPI Library: A Denial of Service/,
      );
    });

    test("has canonical link", async ({ page }) => {
      await expect(page.locator("link[rel=canonical]")).toHaveAttribute(
        "href",
        "https://advisories.gitlab.com/pkg/pypi/github.com/devjoe/fastapi-lib/CVE-2024-65432",
      );
    });

    test("loads Google Analytics", async ({ page }) => {
      const scripts = page.locator("script");
      const srcList = await scripts.evaluateAll((elements) =>
        elements.map((el) => el.getAttribute("src")),
      );
      expect(srcList).toContain(
        `https://www.googletagmanager.com/gtm.js?id=${GTM_ID}`,
      );
    });

    test("loads OneTrust cookie consent", async ({ page }) => {
      const scripts = page.locator("script");
      const srcList = await scripts.evaluateAll((elements) =>
        elements.map((el) => el.getAttribute("src")),
      );
      expect(srcList).toContain(
        `https://cdn.cookielaw.org/consent/${OT_ID}/OtAutoBlock.js`,
      );
      expect(srcList).toContain(
        "https://cdn.cookielaw.org/scripttemplates/otSDKStub.js",
      );
      expect(srcList).toContain(
        "https://cdn.cookielaw.org/scripttemplates/202407.2.0/otBannerSdk.js",
      );
    });
  });

  test.describe("Body", () => {
    test("has page heading", async ({ page }) => {
      await expect(
        page.getByRole("heading", {
          name: "CVE-2024-65432: Denial of Service Vulnerability in FastAPI Library",
          exact: true,
        }),
      ).toBeVisible();
    });

    test("has publication date", async ({ page }) => {
      await expect(
        page.locator("time").filter({ hasText: /August 1, 2024/ }),
      ).toBeVisible();
    });

    test("has updated date", async ({ page }) => {
      await expect(
        page.locator("time").filter({ hasText: /August 2, 2024/ }),
      ).toBeVisible();
    });

    test("has advisory description", async ({ page }) => {
      const content = await page.textContent("article");

      expect(content).toContain(
        "A critical Denial of Service (DoS) vulnerability (CVE-2024-65432) has been discovered in the FastAPI Library, an essential Python library for building high-performance APIs.",
      );
      expect(content).toContain(
        "For more information, please refer to the official advisories and release notes.",
      );
    });
  });
});
