document.addEventListener("DOMContentLoaded", () => {
  // Initialize Pagefind search.
  new PagefindUI({
    element: "#search",
    showEmptyFilters: false,
    sort: {
      date: "desc"
    },
    resetStyles: false,
    debounceTimeoutMs: 500,
    /**
     * HACK: in order to prevent the browser from freezing when terms have too
     * many results, we enforce that terms must have at least 4 characters, and
     * if the term looks like a search for a specific CVE, we require a year and
     * at least one digit of the sequence number.
     *
     * By returning an empty string, PageFind will not invoke the search backend.
     */
    processTerm: function (term) {
      term = String(term).trim();

      if (term.length < 4) {
        return "";
      }

      if (term.toLowerCase().startsWith("cve-")) {
        if (!term.match(/^cve-\d{4}-\d/i)) {
          return "";
        }
      }

      return term;
    },
  });

  // Show mewnu when mobile hamburger menu icon is clicked.
  document.getElementById("navbar-burger").addEventListener("click", (event) => {
    event.preventDefault();
    document.getElementById("navbar-menu").classList.toggle("hidden");
  });
});
